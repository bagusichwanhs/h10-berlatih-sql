1. Membuat Database 
create database myshop;

2. Buat Table di dalam Database
//USER
create table user(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

//CATEGORIES
create table categories(
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );

//ITEMS
create table items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id)
    -> );

3. Memasukkan Data pada Table
//USER
insert into user(name, email, password) values
    -> ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

//CATEGORIES
insert into categories(name) values
    -> ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

//ITEMS
insert into items(name, description, price, stock, category_id) values
    -> ("Sumsang b50", "hp keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 3 rows affected (0.004 sec)

4. Mengambil Data dari Database
//Mengambil Data USERS
select id, name, email from user;

//Mengambil Data ITEMS
//Harga di atas 1000000
select * from items where price > 1000000;
//Name serupa atau mirip
select * from items where name like "%Watch%";

//Menampilkan Data Items Join dengan Kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
    -> from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari Database
select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hp keren dari merek sumsang       | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+